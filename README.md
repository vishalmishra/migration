Useful Links |
📖 [**GCP Migration Project Doc**](https://docs.google.com/document/d/1p3Brri44_SKyakViKB-LGWCmCcwILW6z2A8a8eWFyFc/edit) |
📁 [**GCP Project Docs**](https://drive.google.com/open?id=1mkpbzwJXmALNVYFPC666bh21R05qOF-e) |
📒 [**Demo Scripts**](https://drive.google.com/drive/u/0/folders/1eZGUZPkfUgShND6G9X2B8lwPpGSHv8sY) |
📘 [**Architecture Docs**](https://drive.google.com/drive/u/0/folders/1v-gy_x98FbUi2bemWSgncdv1K4p3OQh1)

# GitLab GCP Migration Project

## Goal

Goals of the GCP Migration Project

In order of descending priority. Most important goals at the top.

1. Use the opportunity of an inter-cloud migration to make GitLab.com suitable for mission critical client workloads
1. Migrate GitLab.com from the Microsoft Azure Cloud platform to the Google Cloud while keeping downtime to a minimum
1. Use the same helm charts for GitLab.com as our EEP customers use
1. The goal here is for customers to be able to spin up a 10 person GitLab EEP instance in Kubernetes and scale it up to 100k users (or more) with little effort.
1. Use the migration as a marketing opportunity for GitLab Inc through creation of technical content

More details are available in the [**GCP Migration Project Doc**](https://docs.google.com/document/d/1p3Brri44_SKyakViKB-LGWCmCcwILW6z2A8a8eWFyFc/edit).

## Project Process

### Demos

This project is demo-driven. The demo takes place at 16h00 UTC every Friday. The demos iterate around a monthly plan with the final demo around the middle of the month (the closest Friday to the 15th).

Each monthly iteration has a demo script. These can be found in [Google Drive](https://drive.google.com/drive/folders/1eZGUZPkfUgShND6G9X2B8lwPpGSHv8sY). Each week the team
will run through the current demo-script and score the functionality being demonstrated. The scoring system is shown below:

#### Demo Scorecard

| **Score**  | [**Label**](https://gitlab.com/gitlab-com/migration/boards/437762) | **Description** |
|---------|-------------|---------------|
| :zero:  |             | Not started or demo-able |
| :one:   | ~"Score:1"  | Demoed buggy, missing functionality |
| :two:   | ~"Score:2"  | Demoed partially, needs polish|
| :three: | ~"Score:3"  | Demoed with complete functionality |
| :four:  |             | Reviewed and merged |

The final score for the demo can then be calculated as a percentage; the idea being that over the month, the score will tend to 100% (indicating the successful completion of all features).

### Planning

The demo-script spreadsheet is [automatically generated](https://gitlab.com/andrewn/gcp-migration-pm) from the issues in this repository. The process is as follows:

1. Each [monthly demo has a milestone](https://gitlab.com/gitlab-com/migration/milestones)
1. Prior to the kickoff of the monthly, the team picks items from [the backlog](https://gitlab.com/gitlab-com/migration/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Demoable&milestone_title=No+Milestone) or create new issues, labelling them ~Demoable.
1. Any issues labelled ~Demoable, that is assigned to the will automatically be added to the demo-script.
1. The description and title of the demo should remain up-to-date as the goal of the demo evolves over the month.

## Related Projects

1. **Cloud Native GitLab Helm Charts**: https://gitlab.com/charts/helm.gitlab.io
1. **Automate the lifecycle of environments for GitLab.com**: https://gitlab.com/gitlab-com/environments
1. **GitLab.com Infrastructure**: https://gitlab.com/gitlab-com/infrastructure
1. **GitLab CE**: https://gitlab.com/gitlab-org/gitlab-ce

## Glossary

The GCP Migration Project involves many different teams within GitLab, as well as external stakeholders.

For this reason, some confusion may arise around different terminology being used by different teams. This glossary is an
effort to define terms for use throughout the project and by all teams.

### D

* **Direct Object Storage**: object storage that does not rely on a shared-filesystem (ie, NFS). Note that the client can communicating directly with the object storage
  or a server-side component (possibly workhorse) can intermediate between the client and the object store.

### E

* **Environment**: a full, production-like environment. Contains it's own k8s cluster, it's own pets (postgres, redis, etc) as well as it's own
  instrumentation and metrics services (prometheus, etc). During the migration process, we will iteratively generate new environments for testing and
  feed any issues back into the environment setup, helm chart development and application development work streams.

### R

* **Review App**: a deployment of the GitLab application running inside an environment. Multiple review apps can run concurrently in a single environment.
